<?php
/**
 * Nola functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

if ( ! function_exists( 'nola_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function nola_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * This theme uses wp_nav_menu() in one location.
		 */
		register_nav_menus( array(
			'menu-1' => esc_html( 'Primary' ),
			'menu-2' => esc_html( 'Footer menu' )
		) );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'post-header-full', 1440, 480, true );
		add_image_size( 'post-preview', 360, 140, true );
	}
endif;

add_action( 'after_setup_theme', 'nola_setup' );


if ( ! function_exists( 'nola_scripts' ) ) :
	function nola_scripts() {

		wp_enqueue_style(
			'nola-libs-css',
			get_template_directory_uri() . '/css/libs.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'nola-styles',
			get_template_directory_uri() . '/css/styles.css',
			array( 'nola-libs-css' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'nola-fonts',
			'https://fonts.googleapis.com/css?family=Libre+Baskerville:700|Open+Sans:400,400i,600',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'nola-style',
			get_stylesheet_uri(),
			array( 'nola-libs-css', 'nola-styles' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_script(
			'nola-libs-js',
			get_template_directory_uri() . '/js/libs.js',
			array( 'jquery' ),
			'1.0.0',
			true
		);

		wp_enqueue_script(
			'nola-scripts-js',
			get_template_directory_uri() . '/js/scripts.js',
			array( 'nola-libs-js' ),
			'1.0.0',
			true
		);

		wp_enqueue_script(
			'nola-theme-js',
			get_template_directory_uri() . '/js/theme.js',
			array( 'nola-scripts-js' ),
			'1.0.0',
			true
		);
	}
endif;

add_action( 'wp_enqueue_scripts', 'nola_scripts' );

function nola_posted_on( $post_date ) {
	$time_string = date( 'd M Y', strtotime( $post_date ) );

	echo $time_string;
}

function nola_recent_posts( $posts_count = 3, $posts_offset = 0, $category_name = 'blog', $order_by = 'date' ) {
	$order = '';
	if ( $order_by == 'date' ) {
		$order = "DESC";
	} elseif ( $order_by == 'title' ) {
		$order = "ASC";
	}
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

	$args  = array(
		'posts_per_page'   => $posts_count,
		'post_type'        => 'post',
		'category_name'    => $category_name,
		'post_status'      => 'publish',
		'paged'            => $paged,
		'orderby'          => $order_by,
		'order'            => $order,
		'offset'           => $posts_offset,
		'suppress_filters' => true

	);
	$query = new WP_Query( $args );
	global $post;

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$post = $query->post;
			setup_postdata( $post );

			if ( has_post_thumbnail( $post->ID ) ) :
				get_template_part( 'template-parts/post-preview' );
			else :
				get_template_part( 'template-parts/post-preview-no-img' );
			endif;
		}
		wp_reset_postdata();
	} else {
		wp_reset_postdata();
	}
}

require 'inc/custom-post-types.php';