<?php get_header(); ?>
<main class="site-main">
	<div class="fullscreen-slider">
		<div class="fullscreen-slide">
			<picture>
				<source srcset="img/index/slide1--1440x1.jpg 1x, img/index/slide1--1440x2.jpg 2x" media="(min-width: 1200px)">
				<source srcset="img/index/slide1--768x1.jpg 1x, img/index/slide1--768x2.jpg 2x" media="(min-width: 768px)">
				<source srcset="img/index/slide1--480x1.jpg 1x, img/index/slide1--480x2.jpg 2x" media="(max-width: 767px)">
				<source srcset="img/index/slide1--320x1.jpg 1x, img/index/slide1--320x2.jpg 2x" media="(max-width: 479px)"><img src="img/index/slide1--1440x1.jpg" alt="">
			</picture>
			<div class="fullscreen-slide-wrapper">
				<div class="fullscreen-slide__text container"><span class="fullscreen-slide__heading">Lorem ipsum dolor sit amet</span><span class="fullscreen-slide__subheading">Donec finibus vulputate nibh quis lacinia. Integer ac efficitur dui, bibendum laoreet purus. Mauris pellentesque ultricies nisi sed dictum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</span><a class="btn btn--white" href="/get-started.html">Get Started</a></div>
			</div>
		</div>
		<div class="fullscreen-slide">
			<picture>
				<source srcset="img/index/slide2--1440x1.jpg 1x, img/index/slide2--1440x2.jpg 2x" media="(min-width: 1200px)">
				<source srcset="img/index/slide2--768x1.jpg 1x, img/index/slide2--768x2.jpg 2x" media="(min-width: 768px)">
				<source srcset="img/index/slide2--480x1.jpg 1x, img/index/slide2--480x2.jpg 2x" media="(max-width: 767px)">
				<source srcset="img/index/slide2--320x1.jpg 1x, img/index/slide2--320x2.jpg 2x" media="(max-width: 479px)"><img src="img/index/slide2--1440x1.jpg" alt="">
			</picture>
			<div class="fullscreen-slide-wrapper">
				<div class="fullscreen-slide__text container"><span class="fullscreen-slide__heading">Lorem ipsum dolor sit amet</span><span class="fullscreen-slide__subheading">Donec finibus vulputate nibh quis lacinia. Integer ac efficitur dui, bibendum laoreet purus. Mauris pellentesque ultricies nisi sed dictum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</span><a class="btn btn--white" href="/get-started">Get Started</a></div>
			</div>
		</div>
	</div>
	<div class="fullscreen-form">
		<div class="container">
			<div class="fullscreen-form__wrapper"><span>This place will connect the iframe with SmartMatchApp form.</span><span>The link and all necessary scripts will be indicated via the admin panel</span></div>
		</div>
	</div>
	<div class="our-services">
		<div class="container"><span class="page-heading">Our Services</span><span class="page-subheading">Suspendisse porta massa lorem, sit amet pellentesque orci pulvinar egestas. Aenean non mauris ac ligula pellentesque euismod sit amet eu risus.</span>
			<div class="our-services__wrapper">
				<div class="service-item">
					<div class="service-item__img">
						<svg xmlns="http://www.w3.org/2000/svg" width="80" height="70" viewbox="0 0 80 70">
							<path fill="#bf1e2e" d="M73 7C61.15-4.87 49.4.31 40 8.75 30.59.31 18.94-5 7 7a23.54 23.54 0 0 0 0 33.5C39.9 70.05 39.89 70 39.89 70s0 .1 33.11-29.53A23.53 23.53 0 0 0 73 7z"></path>
							<path fill="#df2a3c" d="M40 8.73V70c.53-.45 4.41-3.82 33-29.5a23.61 23.61 0 0 0 0-33.55C61.16-4.92 49.42.27 40 8.73z"></path>
						</svg>
					</div><span class="service-item__heading">Professional Matchmaking</span><span class="service-item__text">Personalized matches hand-picked just for YOU. We do all the grunt work so that you can just enjoy the dating process.</span><a class="service-item__link" href="/matchmaking.html">Learn more<span>
                  <svg xmlns="http://www.w3.org/2000/svg" width="5.28" height="10" viewbox="0 0 5.28 10">
                    <path fill="#BF1E2E" d="M4.6 5L.08 9.52a.28.28 0 1 0 .4.4L5.2 5.2a.28.28 0 0 0 0-.4L.48.08A.28.28 0 0 0 .28 0a.28.28 0 0 0-.2.08.28.28 0 0 0 0 .4z"></path>
                  </svg></span></a>
				</div>
				<div class="service-item">
					<div class="service-item__img">
						<svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewbox="0 0 72 72">
							<path fill="#bf1e2e" d="M65.12 11A7.2 7.2 0 0 0 58 18.33V30.2a4.73 4.73 0 0 0 4.67 4.8h4.67A4.73 4.73 0 0 0 72 30.2v-12a7.11 7.11 0 0 0-6.88-7.2z"></path>
							<path fill="#df2a3c" d="M46 9H7a7.06 7.06 0 0 0-7 7.09v48.82A7.06 7.06 0 0 0 7 72h39a7.06 7.06 0 0 0 7-7.09V16.09A7.06 7.06 0 0 0 46 9z"></path>
							<path fill="#ff5d6d" d="M40.64 19H12.36A2.37 2.37 0 0 1 10 16.62v-4.75a7.11 7.11 0 0 1 7.07-7.13h2.76a7.06 7.06 0 0 1 13.34 0h2.76A7.11 7.11 0 0 1 43 11.87v4.75A2.37 2.37 0 0 1 40.64 19zm-25.5 21.16a2.34 2.34 0 0 1-1.66-.69l-4.7-4.71a2.35 2.35 0 1 1 3.32-3.33l3 3 7.74-7.75A2.35 2.35 0 1 1 26.2 30l-9.4 9.42a2.34 2.34 0 0 1-1.66.74zm0 18.84a2.34 2.34 0 0 1-1.66-.69l-4.7-4.71a2.35 2.35 0 0 1 3.32-3.33l3 3 7.74-7.75a2.35 2.35 0 1 1 3.32 3.33L16.8 58.3a2.34 2.34 0 0 1-1.66.7z"></path>
							<path fill="#bf1e2e" d="M41.57 30.74H35.2a2.35 2.35 0 1 1 0-4.71h6.36a2.35 2.35 0 1 1 .01 4.71zm0 9.42H30.43a2.35 2.35 0 1 1 0-4.71h11.14a2.35 2.35 0 1 1 0 4.71zm0 9.42H35.2a2.35 2.35 0 1 1 0-4.71h6.36a2.35 2.35 0 1 1 .01 4.71zm0 9.42H30.43a2.35 2.35 0 1 1 0-4.71h11.14a2.35 2.35 0 1 1 0 4.71z"></path>
							<path fill="#ff5d6d" d="M72 53v6a7.22 7.22 0 0 1-.73 3.18l-2.49 5.07-1.7 3.44a2.31 2.31 0 0 1-4.17 0l-1.7-3.44-2.49-5.07A7.22 7.22 0 0 1 58 59v-6h14z"></path>
							<path fill="#bf1e2e" d="M69 67l-1.79 3.62a2.45 2.45 0 0 1-4.41 0L61 67h8z"></path>
							<path fill="#df2a3c" d="M58 30h14v28H58z"></path>
						</svg>
					</div><span class="service-item__heading">Dating/Relationship Coaching</span><span class="service-item__text">Professional coaching and guidance tailored to your unique goals and specialized needs, no matter what they are.</span><a class="service-item__link" href="/coaching.html">Schedule a consultation<span>
                  <svg xmlns="http://www.w3.org/2000/svg" width="5.28" height="10" viewbox="0 0 5.28 10">
                    <path fill="#BF1E2E" d="M4.6 5L.08 9.52a.28.28 0 1 0 .4.4L5.2 5.2a.28.28 0 0 0 0-.4L.48.08A.28.28 0 0 0 .28 0a.28.28 0 0 0-.2.08.28.28 0 0 0 0 .4z"></path>
                  </svg></span></a>
				</div>
				<div class="service-item">
					<div class="service-item__img">
						<svg xmlns="http://www.w3.org/2000/svg" width="98" height="76" viewbox="0 0 98 76">
							<path fill="#bf1e2e" d="M82.58 34Q84 23.65 84 21.76c0-11.47-4.68-20.05-18-20.05s-18 8.58-18 20.05q0 1.89 1.4 12.24h33.18z"></path>
							<path fill="#df2a3c" d="M35 74.4v-8.63a6.44 6.44 0 0 1 4.42-5.85L55 54a3.22 3.22 0 0 0 2.24-3v-7h18.52v7A3.22 3.22 0 0 0 78 54l15.61 6A6.44 6.44 0 0 1 98 65.77v8.63a1.74 1.74 0 0 1-1.85 1.6h-59.3A1.74 1.74 0 0 1 35 74.4z"></path>
							<path fill="#bf1e2e" d="M93.71 59.54l-11.71-4c-2.39 5.51-8.35 9.07-15.46 9.07s-13.17-3.56-15.57-9.07l-11.66 4A6.52 6.52 0 0 0 35 65.6v8.77A1.75 1.75 0 0 0 36.85 76h59.3A1.75 1.75 0 0 0 98 74.37V65.6a6.54 6.54 0 0 0-4.29-6.06z"></path>
							<path fill="#ff5d6d" d="M61.91 16c-7.8 0-6 4.88-10.56 8-.9.62-2.35 1.18-2.35 2.27v4.12c0 9.05 6.83 17 15.87 17.55A17 17 0 0 0 83 31q-6.53-15-21.09-15z"></path>
							<ellipse fill="#bf1e2e" cx="25" cy="7.5" rx="9" ry="7.5"></ellipse>
							<circle fill="#bf1e2e" cx="25.5" cy="27.5" r="19.5"></circle>
							<path fill="#df2a3c" d="M0 74.5v-8.09a6 6 0 0 1 3.58-5.49l12.64-5.58A3 3 0 0 0 18 52.6V46h15v6.6a3 3 0 0 0 1.79 2.74l12.64 5.58A6 6 0 0 1 51 66.41v8.09a1.5 1.5 0 0 1-1.5 1.5h-48A1.5 1.5 0 0 1 0 74.5z"></path>
							<path fill="#bf1e2e" d="M47.53 61l-9.36-4.15a13.73 13.73 0 0 1-12.67 8.76 13.73 13.73 0 0 1-12.67-8.77L3.46 61A6 6 0 0 0 0 66.51v8A1.49 1.49 0 0 0 1.5 76h48a1.49 1.49 0 0 0 1.5-1.48v-8A6 6 0 0 0 47.53 61z"></path>
							<path fill="#ff5d6d" d="M38.49 30.18C36.08 28.1 30.67 24.43 21.4 23a3 3 0 0 0-3.15 1.59 20.38 20.38 0 0 1-7 7.73A2.91 2.91 0 0 0 10 34.77v.84c0 7.92 9.16 15.5 15 15.6s15-6.55 15-15.09v-2.6a4.4 4.4 0 0 0-1.51-3.34z"></path>
						</svg>
					</div><span class="service-item__heading">Free Database Membership</span><span class="service-item__text">Tired of swiping left and right? Join our membership of successful, commitment-minded singles who would like to be considered as matches for our clients. We will let you know when we think you might be a match for one of our amazing singles. Receive invitations to singles events.</span><a class="service-item__link" href="/get-started.html">Sign up now<span>
                  <svg xmlns="http://www.w3.org/2000/svg" width="5.28" height="10" viewbox="0 0 5.28 10">
                    <path fill="#BF1E2E" d="M4.6 5L.08 9.52a.28.28 0 1 0 .4.4L5.2 5.2a.28.28 0 0 0 0-.4L.48.08A.28.28 0 0 0 .28 0a.28.28 0 0 0-.2.08.28.28 0 0 0 0 .4z"></path>
                  </svg></span></a>
				</div>
			</div>
		</div>
	</div>
	<div class="the-matchmaker">
		<div class="matchmaker-card"><span class="matchmaker-card__heading">The Matchmaker</span><span class="matchmaker-card__text">Suspendisse porta massa lorem, sit amet pellentesque orci pulvinar egestas. Aenean non mauris ac ligula pellentesque euismod sit amet eu risus.</span>
			<div class="matchmaker-card__img">
				<picture>
					<source srcset="img/index/matchmaker-card--x1.jpg 1x, img/index/matchmaker-card--x1.jpg 2x" media="(min-width: 642px)">
					<source srcset="img/index/matchmaker-card--641x1.jpg 1x, matchmaker-card--641x2.jpg 2x" media="(max-width: 641px)"><img src="img/index/matchmaker-card--x1.jpg" alt="">
				</picture>
			</div><span class="matchmaker-card__name">Virginia Erickson</span><a class="matchmaker-card__link" href="/about.html">View Bio<span>
              <svg xmlns="http://www.w3.org/2000/svg" width="5.28" height="10" viewbox="0 0 5.28 10">
                <path fill="#BF1E2E" d="M4.6 5L.08 9.52a.28.28 0 1 0 .4.4L5.2 5.2a.28.28 0 0 0 0-.4L.48.08A.28.28 0 0 0 .28 0a.28.28 0 0 0-.2.08.28.28 0 0 0 0 .4z"></path>
              </svg></span></a>
		</div>
	</div>
	<div class="matchmaker-text">
		<picture>
			<source srcset="img/index/matchmaker-text--1440x1.jpg 1x, img/index/matchmaker-text--1440x2.jpg 2x" media="(min-width: 1200px)">
			<source srcset="img/index/matchmaker-text--992x1.jpg 1x, img/index/matchmaker-text--992x2.jpg 2x" media="(min-width: 768px)">
			<source srcset="img/index/matchmaker-text--480x1.jpg 1x, img/index/matchmaker-text--480x2.jpg 2x" media="(min-width: 480px)">
			<source srcset="img/index/matchmaker-text--320x1.jpg 1x, img/index/matchmaker-text--320x2.jpg 2x" media="(max-width: 479px)"><img src="img/index/matchmaker-text--1440x1.jpg" alt="">
		</picture>
		<div class="matchmaker-text-wrapper"><span class="matchmaker-text__text">Suspendisse porta massa lorem, sit amet pellentesque orci pulvinar egestas. Aenean non mauris ac ligula pellentesque euismod sit amet eu risus. Phasellus et varius mi, vitae lobortis ligula. Curabitur vitae ligula eget purus ornare rhoncus sit amet sit amet nibh.</span><a class="btn btn--white" href="/get-started.html">Get Started</a></div>
	</div>
	<div class="testimonials">
		<div class="container">
			<div class="testimonials-slides">
				<div class="testimonials-item">
					<div class="testimonials-item__photo"><img src="http://placehold.it/70x70" srcset="http://placehold.it/70x70 1x, http://placehold.it/140x140 2x" width="70" height="70"></div><span class="testimonials-item__name">Bobby Robertson</span><span class="testimonials-item__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna libero, tristique faucibus dignissim quis, iaculis vel felis. Integer quis purus lacus. Suspendisse porta nunc quam, vel viverra arcu dictum sit amet. Suspendisse potenti. Phasellus et risus ac justo tincidunt sollicitudin et eget leo.</span>
				</div>
				<div class="testimonials-item">
					<div class="testimonials-item__photo"><img src="http://placehold.it/70x70" srcset="http://placehold.it/70x70 1x, http://placehold.it/140x140 2x" width="70" height="70"></div><span class="testimonials-item__name">Bobby Robertson</span><span class="testimonials-item__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna libero, tristique faucibus dignissim quis, iaculis vel felis. Integer quis purus lacus. Suspendisse porta nunc quam, vel viverra arcu dictum sit amet. Suspendisse potenti. Phasellus et risus ac justo tincidunt sollicitudin et eget leo.</span>
				</div>
				<div class="testimonials-item">
					<div class="testimonials-item__photo"><img src="http://placehold.it/70x70" srcset="http://placehold.it/70x70 1x, http://placehold.it/140x140 2x" width="70" height="70"></div><span class="testimonials-item__name">Bobby Robertson</span><span class="testimonials-item__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna libero, tristique faucibus dignissim quis, iaculis vel felis. Integer quis purus lacus. Suspendisse porta nunc quam, vel viverra arcu dictum sit amet. Suspendisse potenti. Phasellus et risus ac justo tincidunt sollicitudin et eget leo.</span>
				</div>
				<div class="testimonials-item">
					<div class="testimonials-item__photo"><img src="http://placehold.it/70x70" srcset="http://placehold.it/70x70 1x, http://placehold.it/140x140 2x" width="70" height="70"></div><span class="testimonials-item__name">Bobby Robertson</span><span class="testimonials-item__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna libero, tristique faucibus dignissim quis, iaculis vel felis. Integer quis purus lacus. Suspendisse porta nunc quam, vel viverra arcu dictum sit amet. Suspendisse potenti. Phasellus et risus ac justo tincidunt sollicitudin et eget leo.</span>
				</div>
			</div>
			<div class="testimonials-controls">
				<div class="control-prev">
					<svg xmlns="http://www.w3.org/2000/svg" width="14.8" height="28" viewbox="0 0 14.8 28">
						<path fill="#ccc" d="M1.92 14L14.56 1.35A.79.79 0 1 0 13.44.23L.23 13.44a.79.79 0 0 0 0 1.12l13.21 13.2A.8.8 0 0 0 14 28a.77.77 0 0 0 .56-.23.79.79 0 0 0 0-1.12z"></path>
					</svg>
				</div>
				<div class="control-indicator"><span class="control-indicator--current"></span><span>/</span><span class="control-indicator--total"></span></div>
				<div class="control-next">
					<svg xmlns="http://www.w3.org/2000/svg" width="14.8" height="28" viewbox="0 0 14.8 28">
						<path fill="#ccc" d="M12.88 14L.23 26.65a.792.792 0 1 0 1.12 1.12l13.21-13.21a.79.79 0 0 0 0-1.12L1.35.23A.8.8 0 0 0 .8 0a.77.77 0 0 0-.56.23.79.79 0 0 0 0 1.12z"></path>
					</svg>
				</div>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>