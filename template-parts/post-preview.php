<?php
/**
 * Template part for displaying preview of post
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
$content = strip_shortcodes( $post->post_content );
$excerpt = wp_trim_words( $content, $num_words = 15, $more = null );
$time    = date( 'd M Y', strtotime( $post->post_date ) );
?>
<a class="card" href="<?php echo get_permalink( $post->ID ); ?>">
	<?php echo get_the_post_thumbnail( $post->ID, $size = array(
		360,
		140
	), $attr = array( 'class' => "card__img" ) ); ?>
	<span class="card__heading"><?php the_title() ?></span>
	<span class="card__excerpt"><?php echo $excerpt; ?></span>
	<span class="card__date"><?php nola_posted_on( $time ); ?></span>
</a>