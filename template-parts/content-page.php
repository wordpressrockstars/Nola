<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class("article-content text-content"); ?>>
	<div class="container container--ultra-narrow">
		<span class="page-heading"><?php the_title(); ?></span>
		<?php the_content(); ?>
	</div>
</div>
