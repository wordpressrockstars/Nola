<div class="testimonials">
	<div class="container">
		<div class="testimonials-slides">
			<?php
			global $post;
			$args = array( 'posts_per_page' => 99, 'offset' => 0, 'post_type' => 'nola_testimonials' );

			$testimonials = get_posts( $args );
			foreach ( $testimonials as $testimonial ) :
				setup_postdata( $testimonial);
				?>
				<div class="testimonials-item">
					<div class="testimonials-item__photo">
						<?php echo get_the_post_thumbnail( $testimonial->ID, array( 70, 70 ) ); ?>
					</div>
					<h3 class="testimonials-item__name"><?php echo get_the_title($testimonial); ?></h3>
					<p class="testimonials-item__text"><?php echo get_the_content($testimonial); ?></p>
				</div>

			<?php endforeach;
			wp_reset_postdata();
			?>


		</div>
		<div class="testimonials-controls">
			<div class="control-prev">
				<svg xmlns="http://www.w3.org/2000/svg" width="14.8" height="28" viewbox="0 0 14.8 28">
					<path fill="#ccc"
						  d="M1.92 14L14.56 1.35A.79.79 0 1 0 13.44.23L.23 13.44a.79.79 0 0 0 0 1.12l13.21 13.2A.8.8 0 0 0 14 28a.77.77 0 0 0 .56-.23.79.79 0 0 0 0-1.12z"></path>
				</svg>
			</div>
			<div class="control-indicator"><span class="control-indicator--current"></span><span>/</span><span
						class="control-indicator--total"></span></div>
			<div class="control-next">
				<svg xmlns="http://www.w3.org/2000/svg" width="14.8" height="28" viewbox="0 0 14.8 28">
					<path fill="#ccc"
						  d="M12.88 14L.23 26.65a.792.792 0 1 0 1.12 1.12l13.21-13.21a.79.79 0 0 0 0-1.12L1.35.23A.8.8 0 0 0 .8 0a.77.77 0 0 0-.56.23.79.79 0 0 0 0 1.12z"></path>
				</svg>
			</div>
		</div>
	</div>
</div>
