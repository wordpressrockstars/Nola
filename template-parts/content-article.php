<?php
/**
 * Template part for displaying content in article
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class("article-content text-content"); ?>>
	<div class="article-content__title-img">
		<picture>
			<?php echo get_the_post_thumbnail( $post->ID, array( 1440, 480 ) ); ?>
		</picture>
	</div>
	<div class="container container--ultra-narrow">
		<span class="article-content__heading"><?php the_title(); ?></span>
		<span class="article-content__date"><?php nola_posted_on( $post->post_date ); ?></span>
		<?php the_content(); ?>
	</div>
</div>
