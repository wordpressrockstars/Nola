<?php get_header(); ?>
<main class="site-main">
	<div class="blog-content">
		<div class="container">
			<span class="page-heading">Blog</span>
			<span class="page-subheading">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec magna augue, porta a urna eget, aliquet egestas arcu.</span>
			<?php get_template_part( 'template-parts/recent-posts-blog' ); ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>
